[TOC]

# Machado's Hexa Palette

## Split Complementary Color

- <span style="color:#2488bc">**blue**</span>: `#2488bc`
- <span style="color:#bc243c">**red**</span>: `#bc243c`
- <span style="color:#24bc58">**green**</span>: `#24bc58`

## Triadic Color

- <span style="color:#243cbc">**blue**</span>: `#243cbc`
- <span style="color:#bc243c">**red**</span>: `#bc243c`
- <span style="color:#3cbc24">**green**</span>: `#3cbc24`

## Tetradic Color

- <span style="color:#a424bc">**strong magenta**</span>: `#a424bc`
- <span style="color:#bc243c">**strong red**</span>: `#bc243c`
- <span style="color:#3cbc24">**strong lime green**</span>: `#3cbc24`
- <span style="color:#24bca4">**strong cyan**</span>: `#24bca4`

## Coffee

- <span style="color:#6f4e37">**coffee**</span>: `#6f4e37`
- <span style="color:#37586f">**very dark desnaturated blue**</span>: `#37586f`

## Gray

- <span style="color:#e1e1e1">**super light gray**</span>: `#e1e1e1`
- <span style="color:#bbbbbb">**light gray**</span>: `#bbbbbb`
- <span style="color:#a6a6a6">**medium gray**</span>: `#a6a6a6`
- <span style="color:#808080">**gray**</span>: `#808080`
- <span style="color:#5a5a5a">**dark gray**</span>: `#5a5a5a`

## Black & White

- <span style="color:#000000">**black**</span>: `#000000`
- <span style="color:#ffffff">**white**</span>: `#ffffff`

## Color Table

| Name | Hexa code |
| --- | --- |
| blue | #2488bc |
| red | #bc243c |
| green | #24bc58 |
| blue | #243cbc |
| red | #bc243c |
| green | #3cbc24 |
| strong magenta | #a424bc |
| strong red | #bc243c |
| strong lime green | #3cbc24 |
| strong cyan | #24bca4 |
| coffee | #6f4e37 |
| very dark desnaturated blue | #37586f |
| ligh gray | #a6a6a6 |
| medium gray | #bbbbbb |
| gray | #808080 |
| dark gray | #5a5a5a |
| black | #000000 |
| white | #ffffff |

